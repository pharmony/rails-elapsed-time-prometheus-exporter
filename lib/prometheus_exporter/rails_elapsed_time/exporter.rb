# frozen_string_literal: true

module PrometheusExporter
  module RailsElapsedTime
    class Exporter
      attr_accessor :client, :registered_metrics, :registered_requests

      def initialize
        self.client = PrometheusExporter::Client.default
        self.registered_metrics = {}
        self.registered_requests = {}
      end

      def run!
        # See https://api.rubyonrails.org/classes/ActiveSupport/Notifications.html
        ActiveSupport::Notifications.subscribe(
          'start_processing.action_controller',
          method(:start_processing)
        )

        ActiveSupport::Notifications.subscribe(
          'process_action.action_controller',
          method(:process_action)
        )
      end

      def start_processing(_name, start, _finish, id, _payload)
        registered_requests[id] = start
      end

      def process_action(_name, _start, finish, id, payload)
        third_party_total_time = observe_third_party_runtimes_from(payload)
        ruby_runtime = calculate_ruby_runtime_from(id, finish,
                                                   third_party_total_time)
        observe_ruby_runtime_from(ruby_runtime, payload)

        puts "[#{self.class.name}] Request '#{payload[:method]} #{payload[:path]} #{payload[:status]}' completed in #{(third_party_total_time + ruby_runtime).round} ms (Thrid Parties: #{third_party_total_time.round} ms)"

        registered_requests.delete(id)
      end

      private

      def calculate_ruby_runtime_from(id, finish, third_party_total_time)
        ruby_runtime = (finish - registered_requests[id]) * 1_000
        ruby_runtime -= third_party_total_time
        ruby_runtime
      end

      def description_for(metric)
        case metric
        when :db_runtime then 'database queries'
        when :mongoid_runtime then 'MongoDB queries'
        when :ruby_runtime then 'Ruby code'
        when :view_runtime then 'views rendering'
        else
          metric
        end
      end

      def observe_ruby_runtime_from(ruby_runtime, payload)
        prometheus_metric_for(:ruby_runtime).observe(
          ruby_runtime,
          request_keys_from(payload)
        )
      end

      def observe_third_party_runtimes_from(payload)
        retrieve_runtimes_from(payload).reduce(0) do |accumulator, runtime|
          metric = prometheus_metric_for(runtime.first)
          metric.observe(runtime.last || 0, request_keys_from(payload))
          accumulator + (runtime.last || 0)
        end
      end

      def prometheus_metric_for(metric)
        return registered_metrics[metric] if registered_metrics.key?(metric)

        registered_metrics[metric] = client.register(
          :histogram,
          "rails_elapsed_times_#{metric}",
          "Histogram of time spent in #{description_for(metric)}"
        )

        registered_metrics[metric]
      end

      def request_keys_from(payload)
        {
          format: payload[:format],
          method: payload[:method],
          path: payload[:path],
          status: payload[:status]
        }
      end

      def retrieve_runtimes_from(payload)
        payload.reduce({}) do |acc, current|
          next acc unless current.first =~ /_runtime/

          acc.update(current.first => current.last)
        end
      end
    end
  end
end
