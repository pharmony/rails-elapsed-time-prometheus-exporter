# frozen_string_literal: true

module PrometheusExporter
  module RailsElapsedTime
    VERSION = '0.3.0'
  end
end
