# frozen_string_literal: true

require 'prometheus_exporter'
require 'prometheus_exporter/metric'

require_relative 'rails_elapsed_time/exporter'
require_relative 'rails_elapsed_time/version'

PrometheusExporter::Metric::Base.default_prefix = 'rails_elapsed_time'

module PrometheusExporter
  module RailsElapsedTime
  end
end

PrometheusExporter::RailsElapsedTime::Exporter.new.run!
