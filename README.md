# PrometheusExporter::RailsElapsedTime

Extends the [prometheus_exporter](https://github.com/discourse/prometheus_exporter)
gem with [Prometheus](https://prometheus.io/) metrics for the Rails times spent
on view and databases.

More precisely this gem sends the same values you can see from your Rails logs,
so that the following line:

```
Completed 200 OK in 146ms (Views: 140.7ms | ActiveRecord: 0.3ms)
```

will be pushed to Prometheus like that:

```
ruby_rails_elapsed_times_total{type="rails_elapsed_time",format="html",method="GET",path="/",status="200",view_runtime="140.72945050999405794e2",db_runtime="0.35234134"} 1
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'prometheus_exporter-rails_elapsed_time', '~> 0.1'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install prometheus_exporter-rails_elapsed_time

## Usage

Update your [prometheus_exporter](https://github.com/discourse/prometheus_exporter)
initializer, typically from `config/initializers/prometheus_exporter.rb`,
in order to add a `require` to this gem:

```ruby
# frozen_string_literal: true

if defined?(PrometheusExporter)
  require 'prometheus_exporter/middleware'
  require 'prometheus_exporter/rails_elapsed_time'

  # This reports stats per request like HTTP status and timings
  #
  # Type    Name                        Description
  # Counter http_requests_total         Total HTTP requests from web app
  # Summary http_duration_seconds       Time spent in HTTP reqs in seconds
  # Summary http_redis_duration_seconds Time spent in HTTP reqs in Redis, in seconds
  # Summary http_sql_duration_seconds²  Time spent in HTTP reqs in SQL in seconds
  # Summary http_queue_duration_seconds Time spent queueing the request in load balancer in seconds
  Rails.application.middleware.unshift PrometheusExporter::Middleware
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake spec` to run the tests.
You can also run `bin/console` for an interactive prompt that will allow you
to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file
to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab
at https://gitlab.com/pharmony/prometheus_exporter-rails_elapsed_time.
This project is intended to be a safe, welcoming space for collaboration,
and contributors are expected to adhere to
the [code of conduct](https://gitlab.com/pharmony/prometheus_exporter-rails_elapsed_time/-/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of
the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the PrometheusExporter::RailsElapsedTime project's
codebases, issue trackers, chat rooms and mailing lists is expected to follow
the [code of conduct](https://gitlab.com/pharmony/prometheus_exporter-rails_elapsed_time/-/blob/master/CODE_OF_CONDUCT.md).
